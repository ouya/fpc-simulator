const path = require('path')
const webpack = require('webpack')
const nodeExternals = require("webpack-node-externals")

const shared = {
  mode: 'production',
  node: {
    fs: "empty",
    child_process: 'empty'
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      loader: 'babel-loader',
      options: {
        presets: ["@babel/react", ["@babel/preset-env", {
          "modules": false
        }]]
      },
      exclude: /node_modules/
    }]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  }
}

const cloud = {
  ...shared,
  target: 'node',
  node: {
    __dirname: false
  },
  entry: './src/cloud.jsx',
  externals: [nodeExternals()],
  output: {
    libraryTarget: "commonjs",
    filename: 'index.js'
  },
}

// This configuration bundles up our React app so that it can be served to the
// browser.
var browser = {
  ...shared,
  entry: ['babel-polyfill', './src/browser.jsx'],
  output: {
    filename: 'browser.js'
  }
}

module.exports = [cloud, browser]