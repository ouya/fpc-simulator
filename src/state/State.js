import * as Mobx from 'mobx'
import axios from "axios"
import '@babel/polyfill/noConflict'

// This is a simple example of a Mobx store. We will use it to manage the state
// of our app.
class State {
  // The state is instantiated from a JSON object. This is so that we can easily
  // seralize and deserailize the state between the server and the broswer.
  constructor(initialState) {
    Object.assign(this, initialState)
    this.runSimulation = this.runSimulation.bind(this)
  }

  async runSimulation(round) {
    // this.parameters are the parameters from the mobx store
    try {
      let response = await axios.post('https://us-central1-fpc-simulator.cloudfunctions.net/testPython/', {
        round: round,
        gridsize: this.parameters.gridsize,
        a: this.parameters.a,
        b: this.parameters.b,
        beta: this.parameters.beta,
        k: this.parameters.k * this.parameters.gridsize * this.parameters.gridsize, // l: 200
        q: this.parameters.q,
        alpha: this.parameters.alpha,
        qStrategy: this.parameters.qStrategy,
        l: this.parameters.l
      })
      return response.data
    } catch (e) {
      console.log(`😱 Axios request failed: ${e}`)
      return false
    }
  }
  // TODO add the other mutators for the settings
  setGridsize = (newSize) => {
    this.parameters.gridsize = parseInt(newSize)
  }
  setA = (newA) => {
    this.parameters.a = parseInt(newA)
  }
  setB = (newB) => {
    this.parameters.b = parseInt(newB)
  }
  setBeta = (newBeta) => {
    this.parameters.beta = parseInt(newBeta)
  }
  setQ = (newQ) => {
    this.parameters.q = parseInt(newQ)
  }
  setK = (newK) => {
    this.parameters.k = parseInt(newK)
  }
  setAlpha = (newAlpha) => {
    this.parameters.alpha = parseInt(newAlpha)
  }
  setQStrategy = (newQStrategy) => {
    this.parameters.qStrategy = parseInt(newQStrategy)
  }
  setL = (newL) => {
    this.parameters.l = parseInt(newL)
  }
}
// This turns the class into a Mobx class, indicates which fields should be
// observable (i.e. inform components when they update), and which methods
// affect state (we don't use any features that depend on this in this demo).
Mobx.decorate(
  State, {
    parameters: Mobx.observable,
    runSimulation: Mobx.action,
    setGridsize: Mobx.action,
    setA: Mobx.action,
    setB: Mobx.action,
    setBeta: Mobx.action,
    setQ: Mobx.action,
    setK: Mobx.action,
    setAlpha: Mobx.action,
    setQStrategy: Mobx.action,
    setL: Mobx.action
  }
)

export {
  State
}