import React, { Fragment } from 'react'
import * as MobxReact from 'mobx-react'
import mpld3 from './mpld3.js'
import * as d3 from "d3"

import Container from '../ui/Container'
import Row from '../ui/Row'
import Col from '../ui/Col'
import Button from '../ui/Button'
import InputRange from '../ui/InputRange'
import '@babel/polyfill/noConflict'

// Here we define the base class for our Todos component. Mobx will tell the
// component to update whenever any observable Mobx object referenced in the
// class's render method changes. In this case, the todos list.
class BaseClass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      charts: [],
      btnDisabled: false,
      endSimulation: false,
      resultText: ''
    }
  }
  endSimulation = () => {
    this.setState({ endSimulation: true })

  }
  async runSimulation() {
    this.setState({ btnDisabled: true, charts: [], resultText: '' })
    // TODO get parameters from form and run simulation
    // run simulation function from state
    let round = 0
    while (round < 100 && !this.state.endSimulation) {
      let chart = await this.props.state.runSimulation(round)
      // console.log(chart)
      if (typeof chart === 'string' && chart.startsWith('fpc')) { // meaning we have a result message about fpc consensus
        console.log(chart)
        this.setState({ resultText: chart })
        break
      }
      let boxId = `chartBox-${round}`
      let boxElement = (<div key={boxId} className="ml-2"><span className="h4 ml-2">{(round == 0) ? 'Initial configuration' : 'Round ' + round}</span><div className="resultBox" id={boxId} ></div></div>)
      this.setState({ charts: [...this.state.charts, boxElement] })
      // console.log(this.state.charts)
      // draw to container
      mpld3.draw_figure(
        `chartBox-${round}`,
        chart,
        false,
        true)
      round++
    } // end while
    this.setState({ btnDisabled: false, endSimulation: false })
    // THIS IS THE DEMO EXAMPLE FROM demo.html
  }
  render() {
    const { state } = this.props
    // console.log("render main container")
    // console.log(state)
    return (
      <Container fluid className="p-2">
        <Row center className="font-weight-bold mt-2 mb-3">
          <h1 >IOTA Fast Probabilistic Consensus Simulator</h1>
        </Row>
        {/*  start input range / TODO copy this input and add all other missing inputs / getValue prop returns the value on change */}
        <Row>
          <Col lg="3" md="6">
            <Row center>
              Gridsize -> Nodes =
            {state.parameters.gridsize * state.parameters.gridsize}
            </Row>
            <Row center className="text-nowrap">
              <span className="font-weight-bold purple-text mr-2">5</span>
              <InputRange
                min={5}
                max={100}
                value={state.parameters.gridsize}
                getValue={state.setGridsize}
                formClassName="w-75"
              />
              <span className="font-weight-bold purple-text ml-2">100</span>
            </Row>
          </Col>
          <Col lg="3" md="6">
            <Row center>
              Lower threshold in 1st round (a %)
            </Row>
            <Row center className="text-nowrap">
              <span className="font-weight-bold purple-text mr-2">0%</span>
              <InputRange
                min={0}
                max={state.parameters.b}
                value={state.parameters.a}
                getValue={state.setA}
                formClassName="w-75"
              />
              <span className="font-weight-bold purple-text ml-2">100%</span>
            </Row>
          </Col>
          <Col lg="3" md="6">
            <Row center>
              Upper threshold in 1st round (b %)
            </Row>
            <Row center className="text-nowrap">
              <span className="font-weight-bold purple-text mr-2">{state.parameters.a}%</span>
              <InputRange
                min={state.parameters.a}
                max={100}
                value={state.parameters.b}
                getValue={state.setB}
                formClassName="w-75"
              />
              <span className="font-weight-bold purple-text ml-2">100%</span>
            </Row>
          </Col>
          <Col lg="3" md="6">
            <Row center>
              Determination phase length (l)
            </Row>
            <Row center className="text-nowrap">
              <span className="font-weight-bold purple-text mr-2">2</span>
              <InputRange
                min={2}
                max={10}
                value={state.parameters.l}
                getValue={state.setL}
                formClassName="w-75"
              />
              <span className="font-weight-bold purple-text ml-2">10</span>
            </Row>
          </Col>
          <Col lg="3" md="6">
            <Row center>
              Majority of yellow 1s at start (alpha %)
            </Row>
            <Row center className="text-nowrap">
              <span className="font-weight-bold purple-text mr-2">51</span>
              <InputRange
                min={51}
                max={99}
                value={state.parameters.alpha}
                getValue={state.setAlpha}
                formClassName="w-75"
              />
              <span className="font-weight-bold purple-text ml-2">99</span>
            </Row>
          </Col>
          <Col lg="3" md="6">
            <Row center>
              Number of queries each node sends (k)
            </Row>
            <Row center className="text-nowrap">
              <span className="font-weight-bold purple-text mr-2">1%</span>
              <InputRange
                min={1}
                max={99}
                value={state.parameters.k}
                getValue={state.setK}
                formClassName="w-75"
              />
              <span className="font-weight-bold purple-text ml-2">99%</span>
            </Row>
          </Col>
          <Col lg="3" md="6">
            <Row center>
              Threshold from 2nd round onwards (beta)
            </Row>
            <Row center className="text-nowrap">
              <span className="font-weight-bold purple-text mr-2">0%</span>
              <InputRange
                min={0}
                max={50}
                value={state.parameters.beta}
                getValue={state.setBeta}
                formClassName="w-75"
              />
              <span className="font-weight-bold purple-text ml-2">50%</span>
            </Row>
          </Col>
          <Col lg="3" md="6">
            <Row center>
              Proportion of adversary nodes (q)
            </Row>
            <Row center className="text-nowrap">
              <span className="font-weight-bold purple-text mr-2">0%</span>
              <InputRange
                min={0}
                max={50}
                value={state.parameters.q}
                getValue={state.setQ}
                formClassName="w-75"
              />
              <span className="font-weight-bold purple-text ml-2">50%</span>
            </Row>

          </Col>
        </Row>

        {/* end input range  */}
        {state.parameters.q !== 0 ?
          (<Fragment>
            <Row center>
              Adversial strategy 1 or 2 (default 2) , 3-5 with next release
            </Row>
            <Row center className="text-nowrap">
              <span className="font-weight-bold purple-text mr-2">1</span>
              <InputRange
                min={1}
                max={2}
                value={state.parameters.qStrategy}
                getValue={state.setQStrategy}
                formClassName="w-75"
              />
              <span className="font-weight-bold purple-text ml-2">2</span>
            </Row>
          </Fragment>
          ) : null}

        <Row center>
          {this.state.btnDisabled ? (<Fragment>
            <Button size="sm" color="danger" className="ml-2" onClick={() => this.endSimulation()}>stop simulation</Button>
            <div className="spinner-border text-primary ml-2" role="status">
              <span className="sr-only">Calculating...</span>
            </div>
          </Fragment>) :
            (<Button size="sm" color="primary" disabled={this.state.btnDisabled} onClick={() => this.runSimulation()}>run simulation</Button>)}
        </Row>
        {this.state.resultText ? (
          <Col className="h5 text-center mt-2">
            {this.state.resultText}
          </Col>

        ) : null}
        <Row className="mt-3 masonry-with-columns-2">
          {this.state.charts}
        </Row>
      </Container>
    )
  }
}

// Since the Todos component depends on Mobx to tell it when to update - the
// actual class we export is wrapped with Mobx's observer function so that
// Mobx knows to inform the component when to update.
const MainApp = MobxReact.observer(BaseClass)

export { MainApp }
