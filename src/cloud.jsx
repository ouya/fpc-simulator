import fs from 'fs'
import path from 'path'
import React from 'react'
import ReactDOMServer from 'react-dom/server'
import * as Mobx from 'mobx'
import { MainApp } from './components/BaseClass'
import { State } from './state/State'

// Here we simply define the state in a literal.
const state = new State({
  parameters: {
    gridsize: 10,
    a: 55,
    b: 65,
    k: 20,
    beta: 30,
    q: 5,
    alpha: 65,
    qStrategy: 2,
    l: 3
  }
})

// Stringify and encode the state to be injected into LocalStorage.
const dehydrateState = (state) => {
  const stringifiedStore = JSON.stringify(Mobx.toJS(state, true))
  return Buffer.from(stringifiedStore).toString('base64')
}

const bundleUrl = process.env.NODE_ENV === 'production' ? 'https://storage.googleapis.com/fpc-assets/browser.js' : 'browser.js'

// This is the template for the source html we will be sending to the browser.
// In it we have 3 important elements:
// - a div element to insert the React html into
// - a script tag to retrieve our JavaScript so that our app runs in the browser
// - a script to inject our encoded state into LocalStorage
const htmlData = `
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>IOTA Fast Probabilistic Consensus Simulator</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css" integrity="sha256-zmfNZmXoNWBMemUOo1XUGFfc0ihGGLYdgtJS3KCr/l0=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha256-YLGeXaapI0/5IgZopewRJcFXomhRMlYYjugPLSyNjTY=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://storage.googleapis.com/fpc-assets/styles.css"/>
<style>.resultBox{    margin-top: -35px;
    margin-left: -15px;}</style>
  </head>
<body>
    <div id="container"></div>
    <script type="text/javascript" src='${bundleUrl}'></script>
    <script>
      window.localStorage.setItem('state','${dehydrateState(state)}');
    </script>
</body>
</html>
`

const fpc = (req, res) => {
  const html = ReactDOMServer.renderToString(<MainApp state={state} />);

  return res.send(
    htmlData.replace('<div id="container"></div>', `<div id="container">${html}</div>`)
  )
}

// Since we are rendering this in a Google Cloud Function, we use a named exoprt
// that we can reference in our deploy script. This also makes it easy to use
// this funtion in an Express app locally to emulate the cloud during
// development (see local/server.js for more information on this).
export { fpc }
