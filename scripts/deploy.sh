!/usr/bin/env bash
cd dist
#
echo "Deploying assets ..."
echo ""
gsutil cp "./browser.js" gs://fpc-assets/browser.js
gsutil cp "../styles.css" gs://fpc-assets/styles.css
gsutil acl ch -u AllUsers:R gs://fpc-assets/styles.css
gsutil acl ch -u AllUsers:R gs://fpc-assets/browser.js

echo "Deploying functions ..."
echo ""
gcloud beta functions deploy fpc --runtime nodejs8 --trigger-http

cd ..
# gcloud beta functions deploy cloudRender --runtime python37 --trigger-http
