from __future__ import division
import numpy as np
import matplotlib as mpl
# Force matplotlib to not use any Xwindows backend.
mpl.use('Agg')
from matplotlib import pyplot as plt
from matplotlib import animation
import mpld3
from mpld3 import plugins, utils
import json

print(mpl.__version__)
print(mpld3.__version__)


# nb of nodes
gridsize = 10
nodes = gridsize*gridsize

# 1st round thresholds
a = 0.55
b = 0.65

# parameter for test runs
k = 20 # number of queries each node sends
beta = 0.3 # support of the threshold variable from step 2 onwards
q = int(0.1*nodes) # proportion of adversary nodes
l = 3 # length of determination phase
qStrategy = 2

# init nodes - random bit distribution
n = np.random.randint(2, size=nodes) # [0,1,1,0,0,1,1 ....]
# init helper arrays
percentage = [0] * nodes
opinion = [1] * nodes # after having same opinion as step-1, inc 1
qNodes = [0] * q # q adversary nodes
median = int(np.median(n))

def run():
    if np.all(n == 1):
        return 'fpc converged successfully'
    if np.all(n == 0):
        return 'fpc converged with integrity failure'
    if np.sum(opinion) == l*len(opinion):
        return 'fpc terminated: majority is: ' + str(int(np.median(n)))
    for x in range(len(n)):
        # query k nodes - not your own node
        availableNodes = np.arange(nodes)
        availableNodes = np.delete(availableNodes, x)
        queryNodes = np.random.choice(availableNodes, size=k) # choose from all nodes except own node
        honestNodes = np.delete(queryNodes, qNodes) # delete adversary nodes from list
        advNodes = np.delete(queryNodes, honestNodes) # adv nodes which remain un-asked

        # honest nodes response
        percentage[x] = n[honestNodes].sum() / k # percentage of 1s of queryNodes

    # qStrategy - qStrategy 1 adds nothing
    if qStrategy == 2:
        for x in range(len(n)):
            if median==0: # meaning majority of 0s in step-1 then add some 1s to balance out
                percentage[x] += len(advNodes) / k

    if qStrategy == 3:
        # get median of all honest node responses
        interm = np.median(percentage) # TODO check if right for float
        for x in range(len(n)):
            if percentage[x] >= interm:
                percentage[x] += len(advNodes) / k

    # if qStrategy == 4:


    # if qStrategy == 5: # now berserk mode: send 1 and 0s to nodes


    return 0

def adoptFirstRound():
    for x in range(len(n)):
        if percentage[x] > np.random.uniform(a,b,1):
            n[x] = 1
        else:
            n[x] = 0

def adoptFull(): # adds check with determination phase length l
    for x in range(len(n)):
        if opinion[x] < l:
            if percentage[x] > np.random.uniform(beta,1-beta,1): # beta < 0.5
                if n[x]==1:
                    opinion[x] += 1
                else:
                    opinion[x] = 1
                n[x] = 1
            else:
                if n[x]==0:
                    opinion[x] += 1
                else:
                    opinion[x] = 1
                n[x] = 0

fig = plt.figure(figsize=(5,5))
data = np.reshape(n, (gridsize, gridsize))
# G is a NxNx3 matrix
G = np.zeros((gridsize,gridsize,3))
# Where we set the RGB for each pixel
G[data==1] = [0.5,0.5,0.5]
G[data==0] = [0,0,0]
ax = plt.axes()
im = ax.imshow(G,cmap=plt.get_cmap('viridis'),interpolation='none')

def animate(i):
    global median
    runResult = run()
    if (len(str(runResult)) > 1):
        return runResult
    # calc median
    median = int(np.median(n))
    if (i == 1):
        adoptFirstRound()
    if (i > 1):
        adoptFull()
    im.set_data(np.reshape(n, (gridsize, gridsize)))

    return json.dumps(mpld3.fig_to_dict(fig))

def init(alpha):
    global n, nodes, qNodes, percentage, opinion, G, fig, data, im, median
    nodes = gridsize*gridsize
    # init nodes - with a alpha distribution (alpha > 50% on majority 1)
    n = np.zeros(nodes)
    sliceIndex = int(alpha*nodes/100)
    n[:sliceIndex] = 1
    np.random.shuffle(n)
    median = np.median(n)
    if median<1:
        median = int(0)
    else:
        median = int(1)

    # init helper arrays
    percentage = [0] * nodes
    opinion = [1] * nodes # after having same opinion as step-1, inc 1
    qNodes = np.random.choice(nodes, size=q) #TODO size limits

    data = np.reshape(n, (gridsize, gridsize))
    G = np.zeros((gridsize,gridsize,3))
    G[data==1] = [0.5,0.5,0.5]
    G[data==0] = [0,0,0]
    ax.set_ylim(gridsize-0.5, -0.5)
    ax.set_xlim(-0.5, gridsize-0.5)
    im = ax.imshow(G,cmap=plt.get_cmap('viridis'),interpolation='none')



def cloudRender(request):
    request_json = request.get_json()
    if request.method == 'OPTIONS':
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Max-Age': '3600'
        }
        return ('', 204, headers)
    # Set CORS headers for the main request
    headers = {
        'Access-Control-Allow-Origin': '*'
    }

    global nodes, gridsize, a, b, k, beta, q, l
    gridsize = int(request_json['gridsize'])
    nodes = gridsize*gridsize
    a = float(request_json['a']) / 100
    b = float(request_json['b']) / 100
    k = int(round(float(request_json['k']) / 100))
    beta = float(request_json['beta']) / 100
    q = int(float(request_json['q']) / 100 * nodes)
    roundVar = int(request_json['round'])
    l = int(request_json['l'])
    qStrategy = int(request_json['qStrategy'])

    if (roundVar == 0):
        init(float(request_json['alpha'])) # alpha in percent
        im.set_data(np.reshape(n, (gridsize, gridsize)))
        return (json.dumps(mpld3.fig_to_dict(fig)), 200, headers)

    return (animate(roundVar), 200, headers)
